# TASK 2

## INSTALLAZIONE:
- configurare il il file ENV 
- eseguire migrate e seed: è tutto pre-configurato per fare il minimo sforzo 

## NOTE:
Mi sono concentrato sulle direttive, quindi:

- in home ho usato la pagina di default, modificandola giusto per darle un po' di apparenza
- ho usato AUTH di default, modificandolo in minima parte dove serviva
- nella tabella users il seeder genera 4 utenti di test 
    - **un utente ha sempre come mail info@starzero.it ed è l'utente primario con cui testare l'area privata**
    - gli altri utenti hanno una mail generata dal faker, è recuperabile dal db, ed hanno il ruolo sia di amministratori che di user di test per vedere l'area privata da un'altra utenza
    - tutti gli utenti hanno la **password: velvetmedia**

# CSS / JS
- per compilare SASS, concatenare e minimizzare JS è stato usato sia lo strumento di Laravel con "npm run dev", sia *Prepros*: un software dedicato molto più performante

## USABILITÀ, differenti approcci dimostrativi
- progetti:
    - il totale dei progetti associati all'utente è mostrato nella tab
    - i progetti con associato un ticket sono visualizzati per primi in ordine di creazione più recente
    - il resto dei progetti è indicizzato in ordine di creazione più recente
- tickets
    - sulla tab sono visibili il totale dei tickets e la presenza di nuove risposte
    - la scheda è stilata come fosse un'app di messaggistica istantanea, d'altronde l'utente è più a suo agio con interfacce di cui ha già una certa familiarità

## Prioritary TODO
- gestione separata del ticket e dei suoi messaggi, intendendo il ticket come una "conversazione", ottenendo così:
    - gestione degli agganci allo status molto semplificata
    - gestione avanzata di altre funzionalità da applicare al singolo messaggio (new reply, go to messagge, ecc.)
- ottimizzazioni routes
