<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'surname', 'phone_number', 'address', 'building_number', 'postcode', 'city', 'state', 'country',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Projects associated to user
     *
     * @return Illuminate\Database\Eloquent
     */
    public function projects()
    {
        return $this->belongsToMany('App\Project');
    }

    /**
     * Tickets associated to user
     *
     * @return Illuminate\Database\Eloquent
     */
    public function tickets()
    {
        return $this->hasMany('App\Ticket');
    }

}
