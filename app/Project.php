<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Project extends Model
{
    /**
     * This query show a list o project associated to user 
     * and show before the projects with tickets
     * 
     * @return Illuminate\Database\Eloquent
     */
    public function projectsWithActiveTickets()
    {
        return Auth::user()
            // get project associated
            ->projects()
            // get projects with ticket - OLD SCHOOL APPROACH I.E.
            ->leftJoin('tickets', function ($join) {
                $join->on('projects.id', '=', 'tickets.project_id')
                    ->select('id', 'created_at', 'user_id')
                    // only tickets created by logged user
                    ->where('tickets.user_id', Auth::id())
                    // only tickets "root"
                    ->whereNull('ticket_id');
            })
            ->select('projects.id as id', 'projects.name as name', 'tickets.id as ticket_id')
            ->orderBy('tickets.created_at', 'desc')
            ->orderBy('projects.created_at', 'desc')
            ->paginate(config('paginator.rows'));
    }
}
