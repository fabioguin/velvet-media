<?php

namespace App\Helpers;

class DateHelper
{

    /**
     * Quickly parsing data
     *
     * @param  string  $date
     * @param  string  $format
     * @return \Illuminate\Http\Response
     */
    public static function format($date, $format = 'd M y, H:i')
    {
        return \Carbon\Carbon::parse($date)->format($format);
    }

}
