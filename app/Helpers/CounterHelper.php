<?php

namespace App\Helpers;

use App\User as User;
use Auth;

class CounterHelper
{

    /**
     * Count projects by user
     *
     * @return \Illuminate\Http\Response
     */
    public static function projects()
    {
        return Auth::user()->projects()->count();
    }

    /**
     * Count tickets by user
     *
     * @return \Illuminate\Http\Response
     */
    public static function tickets()
    {
        return Auth::user()->tickets()->whereNull('ticket_id')->count();
    }

    /**
     * Count new tickets by user
     *
     * @return \Illuminate\Http\Response
     */
    public static function newTickets()
    {
        return Auth::user()->tickets()->where('new', '=', '1')->count();
    }

}
