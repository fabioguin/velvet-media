<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Ticket extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'message',
    ];

    /**
     * Project associated to ticket
     *
     * @return Illuminate\Database\Eloquent
     */
    public function project()
    {
        return $this->hasOne('App\Project', 'id', 'project_id');
    }

    /**
     * Status associated to ticket
     *
     * @return Illuminate\Database\Eloquent
     */
    public function status()
    {
        return $this->hasOne('App\Status', 'id', 'status_id');
    }

    /**
     * User associated to ticket
     *
     * @return Illuminate\Database\Eloquent
     */
    public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    /**
     * Get list of tickets
     *
     * @return Illuminate\Database\Eloquent
     */
    public function tickets()
    {
        // using "with" for query optimization, so i can select only columns i need
        return $this->with('project:id,name', 'status:id,name,css_class', 'user:id,name,surname')
            ->where('user_id', '=', Auth::id())
            // tickets root
            ->whereNull('ticket_id')
            // news, first
            ->orderBy('new', 'desc')
            ->orderBy('created_at', 'desc')
            ->paginate(config('paginator.rows'));
    }

    /**
     * Get single ticket with messages
     * 
     * @param  int  $projectId
     * @param  int  $ticketId
     * @return Illuminate\Database\Eloquent
     */
    public function ticketWithMessages($projectId, $ticketId)
    {
        return $this->with('project:id,name', 'status:id,name,css_class')
            ->where([
                ['ticket_id', '=', $ticketId],
                ['project_id', '=', $projectId],
            ])
            // show root
            ->orWhere('id', '=', $ticketId)
            ->get();
    }

}
