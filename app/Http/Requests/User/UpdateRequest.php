<?php

namespace App\Http\Requests\User;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:191',
            'surname' => 'required|max:191',
            'phone_number' => 'required|max:20',
            'address' => 'max:191',
            'building_number' => 'max:191',
            'postcode' => 'max:20',
            'city' => 'max:191',
            'state' => 'max:191',
            'country' => 'max:191',
        ];
    }
}
