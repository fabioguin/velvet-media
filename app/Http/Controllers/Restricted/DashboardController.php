<?php

namespace App\Http\Controllers\Restricted;

use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('restricted.dashboard');
    }
}
