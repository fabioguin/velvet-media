<?php

namespace App\Http\Controllers\Restricted;

use App\Http\Controllers\Controller;
use App\Http\Requests\User\UpdateRequest;
use App\User as User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     * Show the form for editing the user.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('restricted.profile')->with(compact('user'));
    }

    /**
     * Update the user in storage.
     *
     * @param  App\Http\Requests\User\UpdateRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $id)
    {
        User::findOrFail($id)->update($request->all());
        return redirect()->route('user.edit', $id)->with('status', 'Profile updated!');
    }

}
