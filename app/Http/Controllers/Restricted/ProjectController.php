<?php

namespace App\Http\Controllers\Restricted;

use App\Http\Controllers\Controller;
use App\Project;

class ProjectController extends Controller
{

    /**
     * Display a listing of the projects associated by user and view ticket opened
     *
     * @param  App\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function index(Project $project)
    {
        return view('restricted.projects', ['projects' => $project->projectsWithActiveTickets()]);
    }

}
