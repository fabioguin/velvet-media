<?php

namespace App\Http\Controllers\Restricted;

use App\Http\Controllers\Controller;
use App\Http\Requests\Ticket\UpdateRequest;
use App\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    /**
     * List of tickets with status and new messages
     *
     * @param  App\Ticket  $ticket
     * @return \Illuminate\Http\Response
     */
    public function index(Ticket $ticket)
    {
        return view('restricted.tickets.index', ['tickets' => $ticket->tickets()]);
    }

    /**
     * Show the form for creating a new ticket.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // TODO
        return view('restricted.tickets.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Single ticket with messages and user name
     *
     * @param  App\Ticket  $ticket
     * @param  int  $projectId
     * @param  int  $ticketId
     * @return \Illuminate\Http\Response
     */
    public function show(Ticket $ticket, $projectId, $ticketId)
    {
        // TODO: separate ticket from messages
        return view('restricted.tickets.show', ['tickets' => $ticket->ticketWithMessages($projectId, $ticketId)]);
    }

    /**
     * Show the form for editing the message
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ticket = Ticket::findOrFail($id);
        return view('restricted.tickets.edit')->with(compact('ticket'));
    }

    /**
     * Update message in storage.
     *
     * @param  App\Http\Requests\Ticket\UpdateRequest  $request
     * @param  int  $projectId
     * @param  int  $ticketId
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, $projectId, $ticketId)
    {
        Ticket::findOrFail($ticketId)->update($request->all());
        // this method could be integrated with an alert for the admin
        // alert colud be a messagge mail or a specific label on the back office
        return redirect()->route('tickets.show', [$projectId, $ticketId])->with('status', 'Message updated!');
    }

}
