<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return view('home');
})->name('home');

// default auth
// NOTE: some vars has been rewrited
Auth::routes();

// Restricted area
Route::group(['prefix' => 'restricted', 'middleware' => ['auth']], function () {

    // landing after login or some other auth action
    Route::get('dashboard', 'Restricted\DashboardController@index')->name('dashboard');
    
    // projects
    Route::get('projects', 'Restricted\ProjectController@index')->name('projects');
    
    // tickets
    Route::group(['prefix' => 'tickets'], function () {
        
        Route::get('', 'Restricted\TicketController@index')->name('tickets.index');
        Route::get('{ticket_id}/edit', 'Restricted\TicketController@edit')->name('tickets.edit');
        
        // ticket by project
        // WARNING: "ticket" is consider a page with all messages; "message" is consider a single message
        Route::group(['prefix' => 'project/{project_id}'], function () {
            Route::get('create', 'Restricted\TicketController@create')->name('tickets.create');
            Route::get('ticket/{ticket_id}', 'Restricted\TicketController@show')->name('tickets.show');
            Route::post('message/{ticket_id}', 'Restricted\TicketController@update')->name('tickets.update');
        });
        
        // status
        Route::group(['prefix' => 'message/{ticket_id}'], function () {
            Route::get('status/{status_id}/change', 'Restricted\StatusController@change')->name('status.change');
        });

    });
    
    // user area
    Route::group(['prefix' => 'users'], function () {
        Route::get('{id}/edit', 'Restricted\UserController@edit')->name('user.edit');
        Route::post('{id}', 'Restricted\UserController@update')->name('user.update');
    });

});
