@extends('layouts.restricted') 

@section('content')

<div class="panel-heading">My tickets</div>

<div class="panel-body">
    @include('restricted.includes.messages')
    @include('restricted.includes.tabs')
</div>

<div class="panel-body">
    
    @if(!empty($tickets))

        <table class="table">
            <thead>
                <tr>
                    <th>#</th>
                    <th>Projects with ticket</th>
                    <th></th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>

                @foreach($tickets AS $ticket)
                
                    <tr>
                        <th scope="row">
                            {{ $ticket->id }}
                        </th>
                        <td>
                            {{ $ticket->project->name }}
                        </td>
                        <td class="text-right">
                            <span class="label label-{{ $ticket->status->css_class }}">
                                {{ $ticket->status->name }}
                            </span> 
                            @if($ticket->new)
                                <span class="label label-new">
                                    New reply
                                </span> 
                            @endif
                        </td>
                        <td class="text-center">
                            <a href="{{ route('tickets.show', [$ticket->project->id, $ticket->id]) }}">
                                <span class="glyphicon glyphicon-comment" data-toggle="tooltip" data-placement="top" aria-hidden="true" title="View ticket"></span>
                            </a>
                        </td>
                    </tr>

                @endforeach

            </tbody>
        </table>
        
        {{ $tickets->links() }} 

    @endif

</div>

@endsection