@extends('layouts.restricted') 

@section('content')

<div class="panel-heading">Edit ticket</div>

<div class="panel-body">
    @include('restricted.includes.messages')
    @include('restricted.includes.tabs')
</div>

<div class="panel-body">

    <form method="post" action="{{ route('tickets.update', [ $ticket->project_id, $ticket->id ]) }}">
        {{ csrf_field() }} 

        <p><small class="text-danger"><strong>*</strong> Required fields</small></p>

        <div class="{{ $errors->has('message') ? ' has-error' : '' }}">
            <label for="_message">Message <strong class="text-danger">*</strong></label>
            <textarea name="message" class="form-control" id="_message" rows="5">{{ old('message', $ticket->message) }}</textarea>
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Save</button>

    </form>

</div>

@endsection