@extends('layouts.restricted') 

@section('content')

<div class="panel-heading">View ticket</div>

<div class="panel-body">
    @include('restricted.includes.messages')
    @include('restricted.includes.tabs')
</div>

<div class="panel-body">

    @foreach($tickets AS $i => $ticket)

        <!-- Project, status and eventually admin new reply notify -->
        @if($i == 0)
            <span class="label label-{{ $ticket->status->css_class }}">
                {{ $ticket->status->name }}
            </span>
            @if($ticket->new)
                <span class="label label-new">
                    New reply
                </span>
            @endif
            <h4>{{ $ticket->project->name }}</h4>
            
            <hr />
        @endif

        <!-- simply 2 css classes is defines on _custom.scss: .ticket, .ticket-inverse -->
        <div class="row ticket {{ $i % 2 == 0 ? '' : 'ticket-inverse' }}">

            <div class="col-md-8">
                {{ $ticket->message }}
            </div>
            <div class="col-md-4">
            
                <!-- name user -->
                @if($ticket->user->id == Auth::id())
                    <p>Me</p>
                @else
                    <p>{{ $ticket->user->name }} {{ $ticket->user->surname }}</p>
                @endif

                <!-- creation date and eventually update date -->
                <small>
                    @if($ticket->created_at != $ticket->updated_at)
                        <span class="glyphicon glyphicon-pencil" data-toggle="tooltip" data-placement="top" aria-hidden="true" title="Updated at: {{ DateHelper::format($ticket->updated_at) }}"></span>
                    @endif
                    {{ DateHelper::format($ticket->created_at) }}
                </small>

                <!-- actions, they change in many different situations -->
                @if($i == count($tickets) - 1)
                    <div class="btn-group">
                        <button type="button" class="btn btn-xs dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Ticket <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">

                            @if($ticket->user->id == Auth::id())
                                <!-- user can edit ticket only if status is yet opened -->
                                @if($ticket->status->id == 2)
                                    <li><a href="{{ route('tickets.edit', $ticket->id) }}">Edit</a></li>
                                @endif
                            @else 
                                <!-- action for reply ticket by admin -->
                                <li><a href="#">Reply</a></li>
                            @endif

                            <!-- if necessary, user can reopen ticket if status has been solved, closed, cancelled  -->
                            @if(in_array($ticket->status->id, [4,5,6]))
                                <li><a href="#">Reopen</a></li>
                            @else 
                                <li><a href="{{ route('status.change', [ $ticket->id, 5 ]) }}">Close</a></li>
                            @endif
                                                           
                            <!-- actions for TESTING! -->
                            @if($ticket->user->id == Auth::id()) 
                                <li role="separator" class="divider"></li>
                                <li><a href="#">Simulate admin answer</a></li>
                                <li><a href="#">Simulate admin change status</a></li>
                            @endif

                        </ul>
                    </div>
                @endif
            </div>
        </div>

    @endforeach

</div>

@endsection