@extends('layouts.restricted')

@section('content')

<div class="panel-heading">Dashboard</div>

<div class="panel-body">
    
    @include('restricted.includes.messages')

    @include('restricted.includes.tabs')

</div>
<div class="panel-body">

    <h2>Hi {{ Auth::user()->name }},</h2>
    <p>you are logged in!</p>

</div>
            
@endsection
