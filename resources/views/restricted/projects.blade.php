@extends('layouts.restricted')

@section('content')

<div class="panel-heading">My projects</div>

<div class="panel-body">
    
    @include('restricted.includes.messages')

    @include('restricted.includes.tabs')

</div>
<div class="panel-body">

    @if(!empty($projects))
        <table class="table">
            <thead> 
                <tr> 
                    <th>#</th> 
                    <th>Project</th> 
                    <th>Actions</th>  
                </tr> 
            </thead> 
            <tbody> 
                @foreach($projects AS $project)
                <tr> 
                    <th scope="row">
                        {{ $project->id }}
                    </th> 
                    <td>
                        {{ $project->name }}
                    </td>
                    <td class="text-center">
                        <!-- open new ticket or read ticket already created -->
                        @empty($project->ticket_id)
                            <a href="{{ route('tickets.create', $project->id) }}">
                                <span class="glyphicon glyphicon-plus" data-toggle="tooltip" data-placement="top" aria-hidden="true" title="Open new ticket"></span>
                            </a>
                        @else
                            <a href="{{ route('tickets.show', [$project->id, $project->ticket_id]) }}">
                                <span class="glyphicon glyphicon-comment" data-toggle="tooltip" data-placement="top" aria-hidden="true" title="View tickets"></span>
                            </a>
                        @endempty
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        {{ $projects->links() }}

    @endif

</div>
            
@endsection
