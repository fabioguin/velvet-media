@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
@if(count($errors->all()) > 0)
    <div class="alert alert-danger" role="alert">
        <p><strong>Ooops!</strong></p>
        <ul>
            @foreach($errors->all() as $e)
                <li>{{$e}}</li>
            @endforeach
        </ul>
    </div>
@endif