<ul class="nav nav-tabs">
    <li role="presentation" class="{{ Route::is('dashboard') ? 'active' : null }}">
        <a  href="{{ route('dashboard') }}">Dashboard</a>
    </li>
    <li role="presentation" class="{{ Route::is('projects') ? 'active' : null }}">
        <a href="{{ route('projects') }}">
            Projects 
            <span class="label label-primary">
                {{ CounterHelper::projects() }}
            </span>
        </a>
    </li>
    <li role="presentation" class="{{ Route::is('tickets.*') ? 'active' : null }}">
        <a href="{{ route('tickets.index') }}" class="label-group">
            Tickets 
            <span class="label label-primary">
                {{ CounterHelper::tickets() }}
            </span>
            @php
                // limit to only one call to db
                $newMsgs = CounterHelper::newTickets();
            @endphp
            @if($newMsgs > 0)
                <span class="label label-new">
                    {{ $newMsgs }}
                </span>
            @endif
        </a>
    </li>
    <li role="presentation" class="{{ Route::is('user.edit') ? 'active' : null }}">
        <a  href="{{ route('user.edit', Auth::user()->id) }}">Profile</a>
    </li>
</ul>