@extends('layouts.restricted')

@section('content')

<div class="panel-heading">My profile</div>

<div class="panel-body">
    
    @include('restricted.includes.messages')

    @include('restricted.includes.tabs')

</div>
<div class="panel-body">

    <form method="post" action="{{ route('user.update', $user->id) }}">
        {{ csrf_field() }} 
        
        <p><small class="text-danger"><strong>*</strong> Required fields</small></p>

        <div class="row">
            <div class="form-group col-md-6 {{ $errors->has('name') ? ' has-error' : '' }}">
                <label for="_name">Name <strong class="text-danger">*</strong></label>
                <input type="text" name="name" class="form-control" id="_name" value="{{ old('name', $user->name) }}" required>
            </div>
            <div class="form-group col-md-6 {{ $errors->has('surname') ? ' has-error' : '' }}">
                <label for="_surname">Surname <strong class="text-danger">*</strong></label>
                <input type="text" name="surname" class="form-control" id="_surname" value="{{ old('surname', $user->surname) }}" required>
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-6">
                <label for="_email">Email address</label>
                <input type="email" class="form-control" id="_email" value="{{ $user->email }}" disabled>
            </div>
            <div class="form-group col-md-6 {{ $errors->has('phone_number') ? ' has-error' : '' }}">
                <label for="_phone_number">Phone number <strong class="text-danger">*</strong></label>
                <input type="tel" name="phone_number" class="form-control" id="_phone_number" value="{{ old('phone_number', $user->phone_number) }}" required>
            </div>
        </div>

        <hr />
        
        <div class="row">
            <div class="form-group col-md-6 {{ $errors->has('address') ? ' has-error' : '' }}">
                <label for="_address">Address</label>
                <input type="text" name="address" class="form-control" id="_address" value="{{ old('address', $user->address) }}">
            </div>
            <div class="form-group col-md-3 {{ $errors->has('building_number') ? ' has-error' : '' }}">
                <label for="_building_number">Building number</label>
                <input type="text" name="building_number" class="form-control" id="_building_number" value="{{ old('building_number', $user->building_number) }}">
            </div>
            <div class="form-group col-md-3 {{ $errors->has('postcode') ? ' has-error' : '' }}">
                <label for="_postcode">Postcode</label>
                <input type="text" name="postcode" class="form-control" id="_postcode" value="{{ old('postcode', $user->postcode) }}">
            </div>
        </div>
        <div class="row">
            <div class="form-group col-md-4 {{ $errors->has('city') ? ' has-error' : '' }}">
                <label for="_city">City</label>
                <input type="text" name="city" class="form-control" id="_city" value="{{ old('city', $user->city) }}">
            </div>
            <div class="form-group col-md-4 {{ $errors->has('state') ? ' has-error' : '' }}">
                <label for="_state">State</label>
                <input type="text" name="state" class="form-control" id="_state" value="{{ old('state', $user->state) }}">
            </div>
            <div class="form-group col-md-4 {{ $errors->has('country') ? ' has-error' : '' }}">
                <label for="_country">Country</label>
                <input type="text" name="country" class="form-control" id="_country" value="{{ old('country', $user->country) }}">
            </div>
        </div>
        <br>
        <button type="submit" class="btn btn-primary">Save</button>
    </form>

</div>
            
@endsection
