<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     * 
     * In this case, i suppose it's not necessary to manage multi address profile. 
     * So, the datas of user_profile can be stored in this table.
     * 
     * Again, the telephone number would be the only necessary data, 
     * but i suppose can be useful to know also the geographical area.
     * 
     * us_en standard - only for e.g.
     * 
     * In a real project i would have associated tables for the city, the country, etc, 
     * and fill the indexed column like a:
     * - city_id
     * - state_id
     * - ...
     * So, the user profile form would can providing a sequential selection fields 
     * managed by ajax or/and RESTful API web service
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            // 
            $table->string('surname')->nullable();
            $table->string('phone_number', 20)->nullable();
            $table->string('address')->nullable();
            $table->string('building_number', 20)->nullable();
            $table->string('postcode', 20)->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
