<?php

use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
 */

$factory->define(App\User::class, function (Faker $faker) {
    return [
        'name' => $faker->firstName(),
        'email' => $faker->unique()->safeEmail,
        'password' => bcrypt('velvetmedia'),
        'remember_token' => str_random(10),
        'surname' => $faker->lastName(),
        'phone_number' => $faker->e164PhoneNumber,
        'address' => $faker->streetSuffix . ' ' . $faker->streetName,
        'building_number' => $faker->buildingNumber,
        'postcode' => $faker->postcode,
        'city' => $faker->city,
        'state' => $faker->state,
        'country' => $faker->country,
        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
    ];
});
