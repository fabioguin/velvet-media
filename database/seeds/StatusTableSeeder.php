<?php

use Illuminate\Database\Seeder;

class StatusTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // default status
        $status = ['To assign' => 'default', 'Open' => 'primary', 'Taken in charge' => 'info', 'Solved' => 'success', 'Closed' => 'danger', 'Canceled' => 'warning'];

        foreach ($status as $name => $css) {
            $seed = App\Status::create([
                'name' => $name,
                'css_class' => $css,
            ]);
        }
    }
}
