<?php

use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class ProjectUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(Faker $faker)
    {
        // retrieve all records from 2 table for
        $projects = App\Project::all()->pluck('id')->toArray();
        $users = App\User::all()->pluck('id')->toArray();
        // number of records is randomize
        // min 40, max is number of projects
        foreach (range(40, count($projects)) as $index) {
            $seed = App\ProjectUser::create([
                // avoid project duplicates for user
                'project_id' => $faker->unique()->randomElement($projects),
                'user_id' => $faker->randomElement($users),
            ]);
        }
    }
}
