<?php

use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class TicketTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @param Faker\Generator  $faker
     * @return void
     */
    public function run(Faker $faker)
    {
        // first level ticket (root), where ticket_id = NULL
        foreach (App\ProjectUser::all()->take(50) as $key => $pivot) {
            $seed = App\Ticket::create([
                'project_id' => $pivot->project_id,
                'user_id' => $pivot->user_id,
                // prefer open status
                'status_id' => collect([1, 2, 2, 2, 2, 3, 4, 5, 6])->random(),
                'message' => $faker->text($maxNbChars = 260),
            ]);
        }
    }
}
