<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Generator as Faker;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * 
     * Insert only 2 user test
     * 
     * @param Faker\Generator  $faker
     * @return void
     */
    public function run(Faker $faker)
    {
        // my user
        // PASSWORD IS ALWAYS velvetmedia
        App\User::create([
            'name' => 'Fabio',
            'email' => 'info@starzero.it',
            'password' => bcrypt('velvetmedia'),
            'remember_token' => str_random(10),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        // user random test us_en
        factory(App\User::class, 3)->create();
    }
}
