<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // don't change the order of classes
        $this->call([
            UserTableSeeder::class,
            ProjectTableSeeder::class,
            ProjectUserTableSeeder::class,
            StatusTableSeeder::class,
            TicketTableSeeder::class,
        ]);
    }
}
